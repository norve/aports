# Contributor: Nick Black <dankamongmen@gmail.com>
# Maintainer: Nick Black <dankamongmen@gmail.com>
pkgname=notcurses
pkgver=3.0.6
pkgrel=0
pkgdesc="blingful character graphics and TUI library"
url="https://nick-black.com/dankwiki/index.php/Notcurses"
arch="all"
license="Apache-2.0"
# FIXME gpm-libs is still in testing. once it moves to community, dep on it,
# and add -DUSE_GPM=on to build().
makedepends="cmake doctest-dev ffmpeg-dev libdeflate-dev libunistring-dev linux-headers ncurses-dev ncurses-terminfo"
subpackages="$pkgname-dbg $pkgname-static $pkgname-dev $pkgname-doc
	$pkgname-libs $pkgname-demo ncneofetch ncls $pkgname-view $pkgname-tetris"
source="https://github.com/dankamongmen/notcurses/archive/v$pkgver/notcurses-$pkgver.tar.gz
	https://github.com/dankamongmen/notcurses/releases/download/v$pkgver/notcurses-doc-$pkgver.tar.gz
	"

[ "$CARCH" = "riscv64" ] && options="textrels"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DUSE_PANDOC=off \
		-DUSE_QRCODEGEN=off \
		$CMAKE_CROSSOPTS
	make -C build
}

check() {
	env TERM=vt100 CTEST_OUTPUT_ON_FAILURE=1 make -C build test
}

package() {
	make -C build DESTDIR="$pkgdir" install
	for i in 1 3 ; do
		find "$srcdir" -maxdepth 1 -type f -iname \*.$i -exec echo "$pkgdir"/usr/share/man/man$i {} \;
		find "$srcdir" -maxdepth 1 -type f -iname \*.$i -exec install -Dm644 -t "$pkgdir"/usr/share/man/man$i {} \;
	done
}

libs() {
	amove usr/lib/libnotcurses*.so.*
}

demo() {
	amove usr/bin/notcurses-demo
	amove usr/bin/notcurses-tester
	amove usr/bin/notcurses-info
	amove usr/share/notcurses
}

ncneofetch() {
	amove usr/bin/ncneofetch
}

ncls() {
	amove usr/bin/ncls
}

view() {
	amove usr/bin/ncplayer
}

tetris() {
	amove usr/bin/nctetris
}

sha512sums="
40ed5175b6c2d3f218e9d0aadd017447102a72debdc06e5d49825c9ddb9aa5e0715b4dc95fa6d4cb443f96f7f0976fd70749db783005a4ba4e3437746bf6522f  notcurses-3.0.6.tar.gz
9c82109b78a8300ece81050ea273b8f5bfda23a4c0a5c99afa763e74b8739cedcad7bbb62ad0c84c5284825562990e0bba6826254c7e656c940a2eb82de282d6  notcurses-doc-3.0.6.tar.gz
"
