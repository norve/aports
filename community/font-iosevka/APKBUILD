# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>

pkgname=font-iosevka
pkgver=14.0.0
pkgrel=0
pkgdesc="Versatile typeface for code, from code."
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	$pkgname-aile
	$pkgname-etoile
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-aile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-etoile-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base
		$pkgname-slab
		$pkgname-curly
		$pkgname-curly-slab
		$pkgname-aile
		$pkgname-etoile
	"

	mkdir -p "$pkgdir"/usr/share/fonts/TTC
	mv "$builddir"/*.ttc "$pkgdir"/usr/share/fonts/TTC
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/TTC/iosevka.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/TTC/iosevka-slab.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/TTC/iosevka-curly.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/TTC/iosevka-curly-slab.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/TTC/iosevka-aile.ttc
}

etoile() {
	pkgdesc="$pkgdesc (Iosevka Etoile)"
	amove usr/share/fonts/TTC/iosevka-etoile.ttc
}

sha512sums="
7b11cb008377982a1012f898915c28cff2868b70913d02c4a325676645eb0d89da7f0bd495cd98f40467c184d0f3fc5027ed7df9942ac211487b5e1db3524df0  super-ttc-iosevka-14.0.0.zip
7e87f0fef1aaa57d70d9e6d7df125d25a40987f7b22a792fbd4dd094ebe3608a423d5f501050a09ada3b0700975316dad1735b8f55eb8294226ea3271446de25  super-ttc-iosevka-slab-14.0.0.zip
1ff086265ed9e6f35ce26a6cb4c32f131661c3ec0bf7fe2922f10f3e6ecdf76b5e89f6556da1b5f091b015bd6e7f84d3c960ad182de7839d12db3144d4a66abc  super-ttc-iosevka-curly-14.0.0.zip
10f8dfa6a18c85308df7b00caf0e2a94d467cf6883e6376981b1df0a2cf5d0e89c5968e5b5e486e6369dc35d4405189ab3acf4968e1012c1aa9dafad043d4106  super-ttc-iosevka-curly-slab-14.0.0.zip
4ce57739a5907391d17b318cba4fc7aa0b305650fd3758733057330b8cc7a796513e77cc73af96b21e4092ffc6aa5573aa9dd6ad9ae8c9826b38c88a15e364a9  super-ttc-iosevka-aile-14.0.0.zip
6c1c928f8b9e38cb44ff59feb25d3e41a575fcc579236a076d4c5b55439ae295f22838d527e82732f646a647aeb50d9d871f508f46083a5b7a1a01d584e66890  super-ttc-iosevka-etoile-14.0.0.zip
"
